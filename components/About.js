const About = () => {
  return (
    <section className="bg-primary pt-14 pb-52">
      <div className="container">
        <div>
          <h2 className="pb-8 text-secondary text-4xl font-bold">About us</h2>
        </div>
        <div className="text-white text-lg leading-loose">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            volutpat nulla est, sit amet semper leo tristique quis. Fusce dolor
            nulla, luctus eget aliquam in, posuere non nisi. Fusce sed quam sed
            tortor ullamcorper condimentum quis et purus. Fusce sed quam sed
            tortor ullamcorper condimentum.
          </p>
          <p className="hidden md:block">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            volutpat nulla est, sit amet semper leo tristique quis. Fusce dolor
            nulla, luctus eget aliquam in, posuere non nisi. Fusce sed quam sed
            tortor ullamcorper condimentum.
          </p>
        </div>
      </div>
    </section>
  );
};

export default About;
