const Footer = ({skew}) => {
  const paddingTop = skew ? "pt-28 2xl:pt-48" : "pt-14";
  return (
    <footer>
      <div className={`bg-primary pb-14 ${paddingTop}`}>
        <div className="container grid grid-cols-1 md:grid-cols-3 gap-4 text-white">
          <div className="flex items-center justify-center">
            <ul className="text-2xl">
              <li className="inline-block p-3">
                <a
                  className="cursor-pointer hover:text-secondary transition duration-300"
                  href="#"
                >
                  <i className="fab fa-instagram"></i>
                </a>
              </li>
              <li className="inline-block p-3">
                <a
                  className="cursor-pointer hover:text-secondary transition duration-300"
                  href="#"
                >
                  <i className="fab fa-twitter"></i>
                </a>
              </li>
              <li className="inline-block p-3">
                <a
                  className="cursor-pointer hover:text-secondary transition duration-300"
                  href="#"
                >
                  <i className="fab fa-linkedin-in"></i>
                </a>
              </li>
              <li className="inline-block p-3">
                <a
                  className="cursor-pointer hover:text-secondary transition duration-300"
                  href="#"
                >
                  <i className="fab fa-telegram"></i>
                </a>
              </li>
            </ul>
          </div>
          <div>
            <ul className="text-lg text-center leading-loose">
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  Address
                </a>
              </li>
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  Phone number
                </a>
              </li>
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  Address
                </a>
              </li>
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  Phone number
                </a>
              </li>
            </ul>
          </div>
          <div>
            <ul className="text-lg text-center leading-loose">
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  Home
                </a>
              </li>
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  About
                </a>
              </li>
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  Book ticket
                </a>
              </li>
              <li>
                <a
                  href="#"
                  className="cursor-pointer hover:text-secondary transition duration-300"
                >
                  subscribe
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="bg-gray-800 py-3">
        <div className="container text-center">
          <span className="text-gray-500">Lorem ipsum dolor sit amet</span>
        </div>
      </div>
    </footer>
  );
};

Footer.defaultProps = {
  skew: false
}

export default Footer;
