import Head from "next/head";

const Header = ({ title, description }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <link rel="icon" href="/images/favicon.png" />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      />
      <meta property="og:title" content="Airplane ticket" />
      <meta
        property="og:image"
        content="/images/hero-bg.jpg"
      />
      <meta property="og:description" content="Airplane ticket" />
      <meta property="og:url" content="/images/hero-bg.jpg" />
      <meta name="twitter:card" content="Airplane ticket" />
    </Head>
  );
};

Header.defaultProps = {
  title: "Airplane",
  description: "airplane ticket",
};

export default Header;
