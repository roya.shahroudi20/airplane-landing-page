import Navbar from "./Navbar";

const Hero = () => {
  return (
    <section className="hero-bg-img w-full h-screen-2/3 sm:h-screen ">
      <Navbar dark="false" />
      <div className="hero-content container flex justify-center items-center">
        <div className="grid-hero border-dashed border-l-4 border-b-4 border-secondary pl-12 pb-8 pr-4 pt-4 grid">
          <div>
            <div className="text-primary text-6xl font-bold pb-7 md:leading-tight">
              <h1>Travel</h1>
              <h1>around the world!</h1>
            </div>
            <div>
              <h2 className="text-white text-lg md:text-2xl font-bold pb-7 leading-tight">
                Where do you want to go?
              </h2>
            </div>
            <div>
              <button className="px-5 md:px-6 py-2 rounded-full bg-secondary text-white font-bold text-base md:text-lg uppercase tracking-wide hover:bg-secondary-dark transition duration-300 focus:outline-none focus:ring-2 focus:ring-secondary-dark focus:border-transparent">
                book ticket
              </button>
            </div>
          </div>
          <div className="hero-plane">
            <img src="/images/plane-hero.png" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
