import Image from "next/image";
import Link from "next/link";

const LoginForm = () => {
  return (
    <section className="bg-gray-200 py-10 min-h-screen flex items-center">
      <div className="container">
        <div className="login-bg bg-white rounded-lg">
          <div className="py-10 px-5 grid grid-cols-1 gap-5 md:grid-cols-2">
            <div className="hidden md:block">
              <Image
                src="/images/login/login.svg"
                width="100"
                height="100"
                layout="responsive"
                className="hover:scale-110 transition transform duration-500"
              />
            </div>
            <div className="flex items-center justify-center">
              <form>
                <div>
                  <h2 className="my-8 font-bold text-3xl text-primary text-center">
                    Login
                  </h2>
                </div>
                <div className="relative">
                  <i className="fa fa-user absolute text-secondary text-xl"></i>
                  <input
                    type="text"
                    placeholder="username"
                    name="username"
                    className="pl-8 border-b-2 font-display focus:outline-none focus:border-secondary transition duration-300 text-lg"
                  />
                </div>
                <div className="relative mt-8">
                  <i className="fa fa-lock absolute text-secondary text-xl"></i>
                  <input
                    type="password"
                    placeholder="password"
                    name="password"
                    className="pl-8 border-b-2 font-display focus:outline-none focus:border-secondary transition duration-300 text-lg"
                  />
                </div>
                <div className="text-center py-5">
                  <Link href="#">
                    <a className="text-primary font-bold">Forgot password?</a>
                  </Link>
                </div>

                <div className="text-center">
                  <button
                    className="py-2 px-20 bg-secondary rounded-full text-white font-bold uppercase text-lg hover:bg-secondary-dark focus:outline-none transition duration-300"
                    type="submit"
                  >
                    Login
                  </button>
                </div>
                <div className="pt-4 text-center">
                  <span className="text-gray-600">
                    Don't have an account?
                    <Link href="/#">
                      <a className="pl-1 text-secondary font-bold">Sign up</a>
                    </Link>
                  </span>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      {/* <img
        src="/images/login/wave.svg"
        className="fixed hidden lg:block -bottom-0"
        style={{"z-index": -1}}
      />

      <div className="w-screen h-screen flex flex-col justify-center items-center lg:grid lg:grid-cols-2">
        <img
          src="/images/login/login.svg"
          className="hidden lg:block w-1/2 hover:scale-150 transition-all duration-500 transform mx-auto"
        />

        <form
          className="flex flex-col justify-center items-center w-1/2"
        >
          <div>
            <h2 className="my-8 font-bold text-3xl text-gray-700 text-center">
              Login
            </h2>
          </div>
          <div className="relative">
            <i className="fa fa-user absolute text-blue-600 text-xl"></i>
            <input
              type="text"
              placeholder="username"
              name="username"
              className="pl-8 border-b-2 font-display focus:outline-none focus:border-blue-600 transition-all duration-500 text-lg"
            />
          </div>
          <div className="relative mt-8">
            <i className="fa fa-lock absolute text-blue-600 text-xl"></i>
            <input
              type="password"
              placeholder="password"
              name="password"
              className="pl-8 border-b-2 font-display focus:outline-none focus:border-blue-600 transition-all duration-500 text-lg"
            />
          </div>
          <a href="#" className="my-4 text-gray-700 font-bold">
            Forgot password?
          </a>

          <button
            onclick="submitLogin()"
            className="py-2 px-20 bg-blue-600 rounded-full text-white font-bold uppercase text-lg mt-4 transform hover:translate-y-1 transition-all duration-500"
            type="submit"
          >
            Login
          </button>

          <span className="py-3">
            Don't have an account yet?
            <a href="/signup" className="my-4 text-gray-700 font-bold">
              Register now
            </a>
          </span>
        </form>
      </div> */}
    </section>
  );
};

export default LoginForm;
