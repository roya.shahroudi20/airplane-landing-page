import Image from "next/image";
import Link from "next/link";

const Navbar = ({ dark }) => {
  const navBg = dark === true ? "bg-primary" : "";
  return (
    <div className={`navbar py-3 ${navBg}`}>
      <div className="container flex justify-between items-center">
        <div>
          <Link href="/">
            <a>
              <Image
                src="/images/favicon-white.png"
                width="40"
                height="40"
                layout="fixed"
              />
            </a>
          </Link>
        </div>
        <nav>
          <ul>
            <li className="mr-2 inline-block text-white md:mr-5">
              <Link href="/">
                <a className="text-lg font-medium capitalize hover:text-secondary-light transition duration-300">
                  home
                </a>
              </Link>
            </li>
            <li className="mr-2 inline-block text-white md:mr-5">
              <Link href="/charts">
                <a className="text-lg font-medium capitalize hover:text-secondary-light transition duration-300">
                  charts
                </a>
              </Link>
            </li>
            <li className="inline-block">
              <Link href="/login">
                <a>
                  <button className="px-4 py-0.5 bg-secondary text-white rounded-full text-lg font-medium capitalize hover:bg-secondary-light focus:outline-none transition duration-300">
                    Login
                  </button>
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

Navbar.defaultProps = {
  dark: true,
};

export default Navbar;
