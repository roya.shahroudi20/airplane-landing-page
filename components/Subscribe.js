const Subscribe = () => {
  return (
    <section className="subscribe-border h-screen-2/3 w-full bg-primary border-dashed border-t-4 border-b-4 border-secondary inline-block py-3">
    <div className="relative subscribe h-full w-full flex items-center">
      <div className="layer"></div>
      <div className="container subscribe-content text-center">
        <h2 className="pb-8 text-primary text-4xl font-bold">Subscribe</h2>
        <h3 className="pb-8 text-lg font-semibold text-primary">
          Be the first to know about the best flights.
        </h3>
        <div>
          <input
            className="py-2 px-4 rounded-l-full text-gray-700 focus:border-primary focus:outline-none focus:ring-2 focus:ring-secondary-light focus:border-transparent w-40 sm:w-52"
            placeholder="Email"
          />
          <button className="bg-secondary text-white py-2 px-5 rounded-r-full placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-secondary-light focus:border-transparent hover:bg-purple transition duration-300">
            <i className="fas fa-paper-plane"></i>
          </button>
        </div>
      </div>
    </div>
    </section>
  );
};

export default Subscribe;
