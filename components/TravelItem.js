import Image from "next/image";

const TravelItem = ({ city, image }) => {
  const imagePath = `/images/${image}`;
  return (
    <div className="pb-5 bg-gray-200 rounded-3xl hover:shadow-lg">
      <div>
        <Image
          src={imagePath}
          width="50"
          height="50"
          layout="responsive"
          className="rounded-t-3xl"
        />
      </div>
      <div className="text-center">
        <h3 className="pt-5 text-secondary font-bold text-xl uppercase">
          {city}
        </h3>
        <p className="pt-3 text-primary-light text-sm">more information...</p>
        <button className="mt-3 bg-primary text-bold text-lg text-white px-5 sm:px-8 py-1 rounded-full hover:bg-primary-dark transition duration-300 focus:outline-none focus:ring-2 focus:ring-primary-light focus:border-transparent">
          book it!
        </button>
      </div>
    </div>
  );
};

export default TravelItem;
