import { Swiper, SwiperSlide } from "swiper/react";

import TravelItem from "../components/TravelItem";

import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";

const TravelLocations = () => {
  return (
    <section className="py-14 bg-gray-100">
      <div className="container">
        <div className="pb-14">
          <h2 className="text-4xl font-bold text-primary text-center">
            Ready to start planing?
          </h2>
        </div>
        <div>
          <Swiper
            slidesPerView={2}
            spaceBetween={10}
            grabCursor={true}
            freeMode={true}
            loop={true}
            centeredSlides={true}
            breakpoints={{
              300: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 4,
                spaceBetween: 30,
              },
            }}
            className="mySwiper"
          >
            <SwiperSlide className="pb-4">
              <TravelItem city="paris" image="Paris.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="Cairo" image="Cairo.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="berlin" image="Berlin.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="tehran" image="Tehran.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="paris" image="Paris.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="Cairo" image="Cairo.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="berlin" image="Berlin.jpg" />
            </SwiperSlide>
            <SwiperSlide className="pb-4">
              <TravelItem city="tehran" image="Tehran.jpg" />
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default TravelLocations;
