import '../styles/tailwind.css'
import '../styles/cssStyles.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
