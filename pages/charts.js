import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import VerticalBar from "../components/charts/VerticalBar";
import Crazy from "../components/charts/Crazy";
import Dynamic from "../components/charts/Dynamic";
import PieChart from "../components/charts/PieChart";
import Polar from "../components/charts/Polar";

export default function Charts() {
  return (
    <div>
      <Header title="Charts" description="airplain ticket charts" />
      <Navbar />
      <main className="">
        <section className="bg-gray-200 min-h-screen flex items-center">
          <div className="container">
            <div className="py-5 grid grid-cols-1 gap-5 md:grid-cols-3">
              <div className="chart-items p-5 rounded-lg">
                <PieChart />
              </div>
              <div className="chart-items p-5 rounded-lg">
                <VerticalBar />
              </div>
              <div className="chart-items p-5 rounded-lg">
                <Polar />
              </div>
            </div>
            <div className="py-5 grid grid-cols-1 gap-5 md:grid-cols-2">
              <div className="chart-items p-5 rounded-lg">
                <Dynamic />
              </div>
              <div className="chart-items p-5 rounded-lg">
                <Crazy />
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </div>
  );
}
