import Header from "../components/Header";
import Hero from "../components/Hero";
import TravelLocations from "../components/TravelLocations";
import About from "../components/About";
import Subscribe from "../components/Subscribe";
import Footer from "../components/Footer";

export default function Home() {
  return (
    <div>
      <Header title="Home" description="airplain ticket" />
      <main className="bg-primary">
        <Hero />
        <TravelLocations />
        <About />
        <Subscribe />
      </main>
      <Footer skew="true" />
    </div>
  );
}
