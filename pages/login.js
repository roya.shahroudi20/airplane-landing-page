import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import LoginForm from "../components/LoginForm";

export default function Login() {
  return (
    <div>
      <Header title="Login" description="login" />
      <Navbar />
      <main>
        <LoginForm />
      </main>
      <Footer />
    </div>
  );
}
