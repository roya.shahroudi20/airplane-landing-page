module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false,
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: '1rem',
        md: '2rem',
      },
    },
    extend: {
      colors: {
        'primary-light': '#3b5166',
        primary: '#0A2540',
        'primary-dark': '#7A73FF',
        'secondary-light': '#4de1ff',
        secondary: '#00D4FF',
        'secondary-dark': '#00aacc',
        purple: '#7A73FF',
      },
      height: {
        "screen-2/3": "75vh"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
